import requests
import unittest

# Variable
base_url = 'http://localhost:5010/leaves'


class Reqtest(unittest.TestCase):

    def test_req(self):
        r = requests.get(base_url)
        self.assertIn('Hello', r.text)
