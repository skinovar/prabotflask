from flask import Flask
from API.LeaveManagement.app import leaves

app = Flask(__name__)

app.register_blueprint(leaves, url_prefix='/leaves')
